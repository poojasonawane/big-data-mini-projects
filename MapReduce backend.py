########################################
## assignment 1, at Stony Brook Univeristy
## Fall 2016

## Pooja Sonawane

## version 1.01
## revision history

from abc import ABCMeta, abstractmethod
from multiprocessing import Process, Manager
from pprint import pprint
import numpy as np
import random
import itertools

##########################################################################
##########################################################################
# PART I. MapReduce

class MyMapReduce:#[TODO]
    __metaclass__ = ABCMeta
    
    def __init__(self, data, num_map_tasks=4, num_reduce_tasks=3):
        #[DONE]
        self.data = data  #the "file": list of all key value pairs
        self.num_map_tasks=num_map_tasks #how many processes to spawn as map tasks
        self.num_reduce_tasks=num_reduce_tasks # " " " as reduce tasks

    ###########################################################
    
    #programmer methods (to be overridden by inheriting class)
    @abstractmethod
    def map(self, k, v): #[DONE]
        print ("Need to override map")
        
    @abstractmethod
    def reduce(self, k, vs): #[DONE]
        print ("Need to override reduce")

    def mapTask(self, data_chunk, map_to_reducer): #[DONE]
        #runs the mappers and assigns each k,v to a reduce task
        for (k, v) in data_chunk:
            #run mappers:
            mapped_kvs = self.map(k, v)
            #assign each kv pair to a reducer task
                
            for (k, v) in mapped_kvs:
                map_to_reducer.append((self.partitionFunction(k), (k, v)))
            
    def partitionFunction(self,k): #[TODO]
        #given a key returns the reduce task to send it
    
        hash_key = sum([ord(c) for c in k])
        node_number = hash_key % self.num_reduce_tasks
        return node_number
    
            
    def reduceTask(self, kvs, from_reducer): #[TODO]
        #sort all values for each key into a list 
        #[TODO]
        tempDict = dict()
        for (k,v) in kvs:
            try:
                tempDict[k].append(v)
            except KeyError:
                tempDict[k] = [v]
                
        #call reducers on each key with a list of values
        #and append the result for each key to reduced_tuples
        #[TODO]
        for (k,v) in tempDict.items():
            from_reducer.append(self.reduce(k,v))
            
    
    ###########################################################

    def runSystem(self): #[TODO]
        #runs the full map-reduce system processes on mrObject

        #the following two lists are shared by all processes
        #in order to simulate the communication
        #[DONE]
        map_to_reducer = Manager().list() #stores the reducer task assignment and 
                                          #each key-value pair returned from mappers
                                          #in the form: [(reduce_task_num, (k, v)), ...]
        from_reducer = Manager().list() #stores key-value pairs returned from reducers
                                        #in the form [(k, v), ...]
        
        #divide up the data into chunks accord to num_map_tasks, launch a new process
        #for each map task, passing the chunk of data to it. 
        #hint: if chunk contains the chunk going to a given maptask then the following
        #      starts a process
        #      p = Process(target=self.mapTask, args=(chunk,map_to_reducer))
        #      p.start()  
        #[TODO]
        chunk_size = int(len(self.data)/self.num_map_tasks)
        
        chunks = [self.data[x:x+chunk_size] for x in range(0, len(self.data), chunk_size)]

        for chunk in chunks:
            p = Process(target=self.mapTask, args=(chunk,map_to_reducer))
            p.start()
            
        #join map task processes back
        #[TODO]
        p.join()

        #print output from map tasks 
        #[DONE]
        print ("map_to_reducer after map tasks complete:")
        pprint(sorted(list(map_to_reducer)))

        #"send" each key-value pair to its assigned reducer by placing each 
        #into a list of lists, where to_reduce_task[task_num] = [list of kv pairs]
        to_reduce_task = [[] for i in range(self.num_reduce_tasks)] 
        #[TODO]
        for each in map_to_reducer:
            to_reduce_task[each[0]].append(each[1])
        #print("to reduce",to_reduce_task)
        
        #launch the reduce tasks as a new process for each. 
        #[TODO]
        for each in to_reduce_task:
            p = Process(target=self.reduceTask, args=(each,from_reducer))
            p.start()

        #join the reduce tasks back
        #[TODO]
        p.join()

        #print output from reducer tasks 
        #[DONE]
        print ("map_to_reducer after reduce tasks complete:")
        pprint(sorted(list(from_reducer)))

        #return all key-value pairs:
        #[DONE]
        return from_reducer


##########################################################################
##########################################################################
##Map Reducers:
            
class WordCountMR(MyMapReduce): #[DONE]
    #the mapper and reducer for word count
    def map(self, k, v): #[DONE]
        counts = dict()
        for w in v.split():
            w = w.lower() #makes this case-insensitive
            try:  #try/except KeyError is just a faster way to check if w is in counts:
                counts[w] += 1
            except KeyError:
                counts[w] = 1
        return counts.items()
    
    def reduce(self, k, vs): #[DONE]
        return (k, np.sum(vs))        
    

class MatrixMultMR(MyMapReduce): #[TODO]
    #the mapper and reducer for matrix multiplication
    def __init__(self, data, num_map_tasks, num_reduce_tasks, mr, nc):
        self.mr = mr
        self.nc = nc
        super().__init__(data, num_map_tasks, num_reduce_tasks)
    def map(self, k, v):
        tuples = dict()
        #print("k v in map: ",k,v)
        if k[0] == 'm':
            for ncol in range(self.nc):
                try:
                    tuples[str((k[1],ncol))].append( (k[0],k[2],v))
                except KeyError:
                    tuples[str((k[1],ncol))] = (k[0],k[2],v)
        if k[0] == 'n':
            for mrow in range(self.mr):
                try:
                    tuples[str((mrow,k[2]))].append((k[0],k[1],v))
                except KeyError:
                    tuples[str((mrow,k[2]))] = (k[0],k[1],v)
        #print("tuples: ",tuples.items)
        return tuples.items()
    
    def reduce(self, k, vs):
        #print("in reduce kvs : ",k , vs)
        mlist = []
        nlist = []   
        prodList = []
        for v in vs:
            if(v[0] == 'm'):
                mlist.append(v)
            else:
                nlist.append(v)
        mlist = sorted(mlist,key=lambda x: x[1])
        nlist = sorted(nlist,key=lambda x: x[1])
        m = dict()
        n = dict()
        for each in mlist:
            m[each[1]] = each [2]
        for each in nlist:
            n[each[1]] = each[2]
        
        for each in m.keys():
            #print("prod of k, v ",k ,mlist[j][2] * nlist[j][2])
            if each in n:
                prodList.append(m[each] * n[each])
        result = np.sum(prodList)
        
        #print("result", k, result)
        
        indices = k.replace('(',' ').replace(')',' ').split(',')
        indices =[int(s.strip()) for s in indices]
        output = ((indices[0],indices[1]),result)  
        
        return output


##########################################################################
##########################################################################
# PART II. Minhashing

def isPrime(n):
    for m in range(2, int(n**0.5)+1):
        if not n%m:
            return False
    return True
    
primes = [i for i in range(1,500) if isPrime(i)]

def hash_func(multiplier, key, n):
    return (((multiplier*key)+random.randint(1,100) % n))

def minhash(documents, k=5): #[TODO]
    #returns a minhashed signature for each document
    #documents is a list of strings
    #k is an integer indicating the shingle size 
    
    nhashes = 100
    
    signatures = None
    
    #Shingle Each Document into sets, using k size shingles
    #[TODO]
    count = 1
    all_shingles = set()
    doc_shingles = dict()
    
    for doc in documents:
        
        #words = doc.split(" ")
        shingles = set()
        for i in range(0,len(doc)-k):
            shingle = ""
            for j in range(0,k):            
                shingle += doc[i+j].lower()
            shingles.add(shingle)
            all_shingles.add(shingle)
        doc_shingles[count] = list(shingles)
        count = count + 1
    
    all_shingles = sorted(all_shingles)
    cm_dict = dict()    
    hashes_dict = dict()
    count = 1
    for shingle in all_shingles:
        hash_row = []
        cm_row = []
        #print (shingle)
        for doc_index in range(1,len(documents)+1):
            
            if (shingle in doc_shingles.get(doc_index)):
                cm_row.append(1)
            else:
                cm_row.append(0)
        
        
        for each in range(1,nhashes+1): 
            hash_row.append(hash_func(random.choice(primes), count, len(all_shingles))) 
        #print(cm_row)
        #print(hash_row)
        hashes_dict[count] = hash_row
        cm_dict[count] = cm_row
        count = count + 1
    
    cm = np.asarray(list(cm_dict.values()))
    hashes = np.asarray(list(hashes_dict.values()))
    
    '''
    print("cm ----\n",cm)
    print("hashes---\n",hashes)
    global globalcm
    globalcm =cm    
    print(cm)
    '''    
    
    
    #Perform efficient Minhash 
    #[TODO]
    signatures = [[np.inf for x in range(len(cm[0]))] for y in range(len(hashes[0]))] 
    signatures = np.asarray(signatures)
    for r in range(len(cm)):
        for s in range(len(cm[r])):
            if cm[r][s] == 1:
                for i in range(len(hashes[r])):
                    if hashes[r][i] < signatures[i][s]:
                        signatures[i][s] = hashes[r][i]
        #print(signatures)
    #Print signatures and return them 
    #[DONE]
    pprint(signatures)
    return signatures #a minhash signature for each document



##########################################################################
##########################################################################

from scipy.sparse import coo_matrix

def matrixToCoordTuples(label, m): #given a dense matrix, returns ((row, col), value), ...
    cm = coo_matrix(np.array(m))
    return  list(zip(zip([label]*len(cm.row), cm.row, cm.col), cm.data))


if __name__ == "__main__":
    ###################
    ##run WordCount:
    data = [(1, "The horse raced past the barn fell"),
            (2, "The complex houses married and single soldiers and their families"),
            (3, "There is nothing either good or bad, but thinking makes it so"),
            (4, "I burn, I pine, I perish"),
            (5, "Come what come may, time and the hour runs through the roughest day"),
            (6, "Be a yardstick of quality."),
            (7, "A horse is the projection of peoples' dreams about themselves - strong, powerful, beautiful"),
            (8, "I believe that at the end of the century the use of words and general educated opinion will have altered so much that one will be able to speak of machines thinking without expecting to be contradicted.")]
    mrObject = WordCountMR(data, 4, 3)
    mrObject.runSystem()
    
    ####################
    ##run MatrixMultiply
    data1 = matrixToCoordTuples('m', [[1, 2], [3, 4]]) + matrixToCoordTuples('n', [[1, 2], [3, 4]])
    data2 = matrixToCoordTuples('m', [[1, 2, 3], [4, 5, 6]]) + matrixToCoordTuples('n', [[1, 2], [3, 4], [5, 6]])
    data3 = matrixToCoordTuples('m', np.random.rand(20,5)) + matrixToCoordTuples('n', np.random.rand(5, 40))
    mrObject = MatrixMultMR(data1, 2, 2, 2, 2)
    mrObject.runSystem() 
    mrObject = MatrixMultMR(data2, 2, 2, 2, 2)
    mrObject.runSystem() 
    
    mrObject = MatrixMultMR(data3, 6, 6, 20, 40)
    mrObject.runSystem() 
    
    ######################
    ## run minhashing:
    documents = ["The horse raced past the barn fell. The complex houses married and single soldiers and their families",
                  "There is nothing either good or bad, but thinking makes it so. I burn, I pine, I perish. Come what come may, time and the hour runs through the roughest day",
                  "Be a yardstick of quality. A horse is the projection of peoples' dreams about themselves - strong, powerful, beautiful. I believe that at the end of the century the use of words and general educated opinion will have altered so much that one will be able to speak of machines thinking without expecting to be contradicted."]
    
    sigs = minhash(documents, 5)
    
    all_combinations = itertools.combinations(range(len(documents)),2)
    '''
    sigs_sent1 = sigs[:,0]    
    sigs_sent2 = sigs[:,1]
    sigs_sent3 = sigs[:,2]
    agree = len(list(set(sigs_sent1) & set(sigs_sent2) & set(sigs_sent3)))
    sim = agree/float(len(sigs))
    print("Estimated similarity for all documents is: {:.2f}".format( sim ))
    '''    
    for each in all_combinations:
        sent1 = each[0]
        sent2 = each[1]
        
        sigs_sent1 = sigs[:,sent1]
        sigs_sent2 = sigs[:,sent2]
        
        agree = len(list(set(sigs_sent1) & set(sigs_sent2)))
        sim = agree/float(len(sigs))
        print("Estimated similarity for document {} and {} is: {:.2f}".format(sent1+1, sent2+1, sim ))
        
        '''
        true_agree = 0
        true_all =  0
        for each in globalcm:
            if each[sent1] == 1 and  each[sent2] == 1:
                true_agree = true_agree + 1
                true_all = true_all + 1
            if each[sent1] == 1 and  each[sent2] == 0:
                true_all = true_all + 1
            if each[sent1] == 0 and  each[sent2] == 1:
                true_all = true_all + 1         
                
        true_sim = true_agree/float(true_all)
        print("Real similarity for document {} and {} is: {:.2f}".format(sent1+1, sent2+1, true_sim ))
        '''
       