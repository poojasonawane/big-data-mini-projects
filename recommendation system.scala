//SETTINGS:
val targetItem = "B000035Y4P"
val ratingsFile = sc.textFile("hdfs:/ratings/ratings_Video_Games.csv.gz")

//val targetItem = "1400599997"
//val ratingsFile = sc.textFile("hdfs:/ratings/ratings_Electronics.csv.gz")

////////////////////////////////
//Methods:
def normalize(row: Array[Double]):Seq[Double] = {
  //subtracts the mean
  val  mean = row.sum / row.length.toFloat
  return row.map(x => x - mean)
}

def cosineSim(x: (Array[Double], Array[Double])): Double = {
  require(x._1.size == x._2.size);
  val z = x._1
  val y = x._2
  val dp = (for((a, b) <- z zip y) yield a * b) sum;
  return dp / (math.sqrt(z map(i => i*i) sum) * math.sqrt(y map(i => i*i) sum));
}

 
///////////////////////////////
//MAIN: 

//get valid items and reduce ratings to those in the list
val ratings = ratingsFile.map(l => l.split(",")).cache()
val validItems = sc.broadcast(ratings.map(x => (x(1), 1)).reduceByKey(_+_).filter(_._2 >= 25).map(x => x._1).collect().toSet)
val ratingsItemFiltered = ratings.filter(x => validItems.value.contains(x(1)))

//get valid users based on restriction and produce working dataset (ratingsFiltered)
val validUsers = sc.broadcast(ratingsItemFiltered.map(x => (x(0), 1)).reduceByKey(_+_).filter(_._2 >= 10).map(x => x._1).collect().toSet)
val ratingsFiltered = ratingsItemFiltered.filter(x => validUsers.value.contains(x(0)))
ratings.unpersist()//won't be needing again

//create item-item Utility Matrix as a data Frame
import org.apache.spark.sql.types._
val utilityMatrix = ratingsFiltered.map(x => (x(1), Map(x(0)->x(2)))).reduceByKey(_ ++ _).cache()
//normalize and convert to Utility matrix
val normUM = utilityMatrix.map(x => (x._1, (x._2.keys zip normalize(x._2.values.toArray.map(_.toDouble))).toMap))
val unnormUM = utilityMatrix.map(x => (x._1, (x._2.keys zip x._2.values.toArray.map(_.toDouble)).toMap))

//search for those matching columns of the target row and intersect columns:
val targetRow = sc.broadcast(normUM.filter(_._1 == targetItem).collect()(0)._2)
val targetRowKeys = sc.broadcast(targetRow.value.keySet)//small enough; to save recomputing
val potentialNeighbors = normUM.map(x => (x._1, x._2.filter(y => targetRowKeys.value.contains(y._1)))).filter(x => x._2.values.toArray.distinct.size >= 2)

//find similarity between target and potential Neighbors
val neighSims = potentialNeighbors.map(x => (x._1, cosineSim(x._2.toArray.map(y => (x._2(y._1), targetRow.value(y._1))).unzip)))
val validNeighSims = sc.broadcast(neighSims.filter(x => x._2 > 0).collect.toMap)

//predict values. 
val neighUIRs= unnormUM.filter(x => (validNeighSims.value.contains(x._1) && validNeighSims.value(x._1) > 0)).flatMap(x => x._2.map(z => (z._1, (validNeighSims.value(x._1), z._2, 1))))
val weightedSumCount = neighUIRs.reduceByKey((x, y) => (x._1+y._1, x._1*x._2+ y._1*y._2, x._3+y._3)).filter(x => x._2._3 > 1).mapValues(x => (x._2 / x._1, x._3))
weightedSumCount.map{case (x, (y, z)) => (x, y)}.collect