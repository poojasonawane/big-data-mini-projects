/***********************************************************
*	Big Data Analytics
* 	Assignment 2 - Part I
*	Pooja Sonawane
*	110739621
*
* The program is run for first 5000 files from CommonCrawl
*
************************************************************/

import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf

object ContextualWordCount {
	def main(args: Array[String]) {
		val conf = new SparkConf().setAppName("Contextual Word Count 5000 files")
		val sc = new SparkContext(conf)
		
		val pathData = sc.textFile("hdfs:/data/wet.paths.5000")
		val path = "s3://commoncrawl/"
		val data = sc.textFile(pathData.map(x => path+x).reduce((a,b) => a+','+b))
		
		val ifeelData = data.map(ContextualWordCount.regMatch)
		val reducedFeelData = ifeelData.flatMap(x => x).map(m => (m.split(' ')(2),1)).reduceByKey(_+_)
		val reducedFeelData_30 = reducedFeelData.filter{case (key, value) => value > 30}
		println(reducedFeelData_30.collect().mkString(", "))
		
		/* save output */
		reducedFeelData_30.saveAsTextFile("hdfs:/output/output5000")
	
	}
	/* regex to extract pattern */
	def regMatch(s: String) : List[String] = {
		val regex = "(^i feel |i feel )\\s*(\\w+)".r
		val result = regex.findAllIn(s.toLowerCase())
		result.toList
	}
}
