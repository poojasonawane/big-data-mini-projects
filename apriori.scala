//a priori

/////////////////////////////////////////
// SETTINGS
//

//val ratingsFile = sc.textFile("hdfs:/ratings/ratings_Video_Games.csv.gz")
val ratingsFile = sc.textFile("hdfs:/ratings/ratings_Electronics.csv.gz")
//val ratingsFile = sc.textFile("s3://bdanalytics-a2/reviews/ratings_Video_Games.csv.gz")

val supThresh = 10
val confThresh = .05
val intThresh = .02

/////////////////////////////////////////
// Methods

def containsAllSubsets(subsets: Iterator[Set[String]], otherSets: Set[Set[String]]): Boolean = {
  for (ss <- subsets){
    if (otherSets.contains(ss) == false){
      return false}
  }
  return true
}

def aPrioriPass(sc: org.apache.spark.SparkContext, b: org.apache.spark.rdd.RDD[(String,Set[String])], km1s: org.apache.spark.broadcast.Broadcast[Set[Set[String]]], k:Int, supThresh:Int): 
  org.apache.spark.broadcast.Broadcast[Set[Set[String]]] = {

  //get possible ksets from baskets meeting criteria from previous pass (km1sets)
  val km1s_items = sc.broadcast(km1s.value.flatten.toSet) //limit to items from the previous kset (reduces the size of subsets substantially)
  val Csets = b.map(x => (x._1, x._2.filter(s => km1s_items.value.contains(s)))).flatMap(x => x._2.subsets(k)).filter(x => containsAllSubsets(x.subsets(k-1), km1s.value))

  //filter to those passing threshold at this k
  val Lsets = Csets.map((_, 1)).reduceByKey(_+_).filter(_._2 >= supThresh).map(_._1)

  return sc.broadcast(Lsets.collect().toSet)
}

def setsOfSingles( s: Set[String]): Set[(Set[String], String)] = {
  val a = s.toArray
  return (0 to a.size-1).map(i => (a.slice(0, i) ++ a.slice(i+1,a.size) toSet, a(i))).toSet
}



///////////////////////////////
//MAIN: 

//get valid items and reduce ratings to those in the list
val ratings = ratingsFile.map(l => l.split(",")).cache()
val itemCounts = ratings.map(x => (x(1), 1)).reduceByKey(_+_)
val validItems = sc.broadcast(itemCounts.filter(_._2 >= supThresh).map(x => x._1).collect().toSet)
val ratingsItemFiltered = ratings.filter(x => validItems.value.contains(x(1)))
println("validItems.size:" + validItems.value.size)

//get valid users based on restriction and produce working dataset (ratingsFiltered)
val validUsers = sc.broadcast(ratingsItemFiltered.map(x => (x(0), 1)).reduceByKey(_+_).filter(_._2 >= 5).map(x => x._1).collect().toSet)
val ratingsFiltered = ratingsItemFiltered.filter(x => validUsers.value.contains(x(0))).cache()

//create baskets
val baskets = ratingsFiltered.map(x => (x(0), x(1))).groupByKey().map(x => (x._1, x._2.toSet)).cache()

///////////////
//run a priori:
var km1sets = sc.broadcast(validItems.value.map(Set(_)))//setup initial km1set (i.e. l1)
var threeSets = sc.broadcast(Set(Set("")))
println("k=1:" + km1sets.value.size)
for( k <- 2 to 4){
  km1sets = aPrioriPass(sc, baskets, km1sets, k, supThresh)
  println("k=" + k + ":" + km1sets.value.size)
  if (k == 3) {threeSets = km1sets}
}
val fourSets = km1sets

////////////////
//Calculate and filter based on confidence and interest
val fourset_items = sc.broadcast(fourSets.value.flatten.toSet)
val totalBaskets = baskets.count
val oneSetCounts = sc.broadcast(itemCounts.filter(s => fourset_items.value.contains(s._1)).collect.toMap)
val threeSetCounts = sc.broadcast(baskets.flatMap(_._2.filter(s => fourset_items.value.contains(s)).subsets(3)).filter(x => threeSets.value.contains(x)).map((_, 1)).reduceByKey(_+_).collect().toMap)
val fourSetCounts = sc.broadcast(baskets.flatMap(_._2.filter(s => fourset_items.value.contains(s)).subsets(4)).filter(x => fourSets.value.contains(x)).map((_, 1)).reduceByKey(_+_).collect().toMap)

//Turn foursets into rdd in order to parallelize confidence and interest filters:
val fourSetRDD = sc.parallelize(fourSets.value.toList)
val CIs = fourSetRDD.map(f => setsOfSingles(f).filter(pair => ((fourSetCounts.value(f) / threeSetCounts.value(pair._1)) > .05) && (((fourSetCounts.value(f) / threeSetCounts.value(pair._1)) - (oneSetCounts.value(pair._2) / totalBaskets)) > .02))).filter(_.nonEmpty)
CIs.collect